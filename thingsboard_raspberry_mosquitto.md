# ThingsBoard, Raspberry Pi and mosquitto-clients

This document intents to help you sending data from a Raspberry Pi 3 to [ThingsBoard live demo server](https://demo.thingsboard.io/) using MQTT. It's supposed to be a simple test to check if your Raspberry Pi is correctly connected to ThingsBoard. You should have Raspberry Pi OS (frmerly Raspbian) running in your Raspberry Pi.

* Access [ThingsBoard live demo server](https://demo.thingsboard.io/).
* Create a device (see step 1 of [Getting Started with ThingsBoard](https://thingsboard.io/docs/getting-started-guides/helloworld/)).
* Copy access token to a safe place (see step 2 of [Getting Started with ThingsBoard](https://thingsboard.io/docs/getting-started-guides/helloworld/)).
* In your Raspberry Pi, install mosquitto mqtt client for Ubuntu:
```
sudo apt-get install mosquitto-clients
```
* Publish temperature data using the command above, replacing $ACCESS_TOKEN for the token you coppied in the previous steps. Be careful with the message part, there sould be no space between **"temperature":** and **25**. 
```
mosquitto_pub -d -q 1 -h "demo.thingsboard.io" -p "1883" -t "v1/devices/me/telemetry" -u "$ACCESS_TOKEN" -m {"temperature":25}
```
* Create a dashboard in [ThingsBoard live demo server](https://demo.thingsboard.io/) (see steps 3.1-3.3 of [Getting Started with ThingsBoard](https://thingsboard.io/docs/getting-started-guides/helloworld/))
* Check if data is correctly published to ThingsBoard. You should things changing in your dashboard as you send more data.
